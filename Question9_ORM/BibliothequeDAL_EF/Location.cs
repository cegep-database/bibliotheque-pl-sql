﻿using System;
using Oracle.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.Json;
using BibliothequeEntite;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace BibliothequeDAL_EF
{
    public class Location
    {
        [Key]
        [Column("ISBN")]
        public string ISBN { get; private set; }
        [Column("TYPEARTICLE")]
        public string TypeArticle { get; private set; }
        [Column("TITRE")]
        public string Titre { get; private set; }
        [Column("NOMBREEMPRUNTS")]
        public int NombreEmprunts { get; private set; }
        public Location()
        {
            ;
        }
        public BibliothequeEntite.Location VersEntite()
        {
            return new BibliothequeEntite.Location(new BibliothequeEntite.Article(this.ISBN, this.TypeArticle, this.Titre), this.NombreEmprunts);
        }
    }
}
