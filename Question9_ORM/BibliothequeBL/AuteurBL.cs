﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliothequeEntite;

namespace BibliothequeBL
{
    public class AuteurBL
    {
        private IDepotAuteur m_DepotAuteur;
        public AuteurBL(IDepotAuteur p_depotAuteur)
        {
            this.m_DepotAuteur = p_depotAuteur;
        }
        public List<Auteur> ListerAuteurs()
        {
            return this.m_DepotAuteur.ListerAuteurs();
        }

    }
}
