﻿using System;
using Oracle.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.Json;
using BibliothequeEntite;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BibliothequeDAL_EF
{
    [Table("BI_EMPRUNTS")]
    public class Emprunt
    {
        [Key]
        [Column("NOMEMBRE")]
        public int NoMembre { get; private set; }
        [Column("ISBN")]
        public string ISBN { get; private set; }
        [Column("NOARTICLE")]
        public int NoArticle { get; private set; }
        public Emprunt()
        {
            ;
        }
        public Emprunt(BibliothequeEntite.Emprunt p_emprunt)
        {
            if(p_emprunt == null)
            {
                throw new ArgumentNullException(nameof(p_emprunt));
            }
            this.NoMembre = p_emprunt.NoMembre;
            this.ISBN = p_emprunt.ISBN;
            this.NoArticle = p_emprunt.NoArticle;
        }
        public BibliothequeEntite.Emprunt VersEntite()
        {
            return new BibliothequeEntite.Emprunt(this.NoMembre, this.ISBN, this.NoArticle);
        }
    }
}
