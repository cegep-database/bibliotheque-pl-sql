﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliothequeEntite;

namespace BibliothequeBL
{
    public class EmpruntBL
    {
        private IDepotEmprunt m_depotEmprunt;
        public EmpruntBL(IDepotEmprunt p_depotEmprunt)
        {
            this.m_depotEmprunt = p_depotEmprunt;
        }
        public void AjouterEmprunt(BibliothequeEntite.Emprunt p_emprunt)
        {
            if(p_emprunt == null)
            {
                throw new ArgumentNullException(nameof(p_emprunt));
            }

            this.m_depotEmprunt.AjouterEmprunt(p_emprunt);
        }
        public BibliothequeEntite.Emprunt CreerEmprunt(int p_numeroMembre, string p_ISBN)
        {
            if (p_numeroMembre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_numeroMembre));
            }
            if (p_ISBN == null)
            {
                throw new ArgumentNullException(nameof(p_ISBN));
            }
            if (p_ISBN.Length != 17)
            {
                throw new ArgumentException(nameof(p_ISBN));
            }
            
            return this.m_depotEmprunt.CreerEmprunt(p_numeroMembre, p_ISBN);
        }
    }
}
