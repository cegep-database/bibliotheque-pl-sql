﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BibliothequeDAL_EF
{
    public class DepotEmprunt : BibliothequeEntite.IDepotEmprunt
    {
        private ApplicationDBContext m_dbContext;

        public DepotEmprunt(ApplicationDBContext p_applicationDBContext)
        {
            if (p_applicationDBContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDBContext));
            }

            this.m_dbContext = p_applicationDBContext;
        }
        public void AjouterEmprunt(BibliothequeEntite.Emprunt p_emprunt)
        {
            BibliothequeDAL_EF.Emprunt emprunt = new Emprunt(p_emprunt);
            this.m_dbContext.Add(emprunt);
            this.m_dbContext.SaveChanges();
            this.m_dbContext.ChangeTracker.Clear();
        }
        public BibliothequeEntite.Emprunt CreerEmprunt(int p_numeroMembre, string p_ISBN)
        {
            if (p_numeroMembre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_numeroMembre));
            }
            if (p_ISBN == null)
            {
                throw new ArgumentNullException(nameof(p_ISBN));
            }
            if (p_ISBN.Length != 17)
            {
                throw new ArgumentException(nameof(p_ISBN));
            }
            List<Emprunt> requete = this.m_dbContext.ObtenirEmprunt(p_numeroMembre, p_ISBN);
            BibliothequeEntite.Emprunt emprunt = requete.Select(emprunt => emprunt.VersEntite()).FirstOrDefault();
            return emprunt;
        }
    }
}
