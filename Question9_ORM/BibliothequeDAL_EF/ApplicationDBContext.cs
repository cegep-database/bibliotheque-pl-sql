﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Oracle.EntityFrameworkCore;
using BibliothequeEntite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeDAL_EF
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions p_dbContextOptions) : base(p_dbContextOptions)
        {
            ;
        }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Auteur> Auteurs { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Emprunt> Emprunts { get; set; }
        public List<Auteur> ObtenirAuteursDeLArticle(string p_ISBN)
        {
            return this.Auteurs.FromSqlRaw("SELECT * FROM TABLE(fct_ListerAuteurs({0}));", p_ISBN).ToList();
        }
        public List<Location> ObtenirNombreDeLocations(int p_mois, int p_annee)
        {
            if(p_mois < 0 || p_mois > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(p_mois));
            }
            if(p_annee < 1900 || p_annee > DateTime.Now.Year)
            {
                throw new ArgumentOutOfRangeException(nameof(p_annee));
            }

            return this.Locations.FromSqlRaw("SELECT * FROM TABLE(FCT_07Best({0}, {1}));", p_mois, p_annee).ToList();
        }
        public List<Emprunt> ObtenirEmprunt(int p_numeroMembre, string p_ISBN)
        {
            if (p_numeroMembre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_numeroMembre));
            }
            if (p_ISBN == null)
            {
                throw new ArgumentNullException(nameof(p_ISBN));
            }
            if (p_ISBN.Length != 17)
            {
                throw new ArgumentException(nameof(p_ISBN));
            }
            return this.Emprunts.FromSqlRaw("SELECT * FROM TABLE(fct_CreerModeleEmprunt({0}, {1}));", p_numeroMembre, p_ISBN).ToList();
        }
    }
}
