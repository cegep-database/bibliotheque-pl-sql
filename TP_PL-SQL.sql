-- Avant de referencer un type objet dans la declaration d'un package, il faut d'abord le creer;
DROP TYPE LISTELOCATIONARTICLE;
CREATE OR REPLACE TYPE LigneLocation IS OBJECT(
    ISBN CHAR(17),
    TypeArticle VARCHAR2(20),
    Titre VARCHAR2(100),
    NombreEmprunts NUMBER
    )
/
CREATE OR REPLACE TYPE LigneElement IS OBJECT(
    Intitule VARCHAR2(100)
    ,Comptes NUMBER
)
/

CREATE OR REPLACE PACKAGE pkg AS
    TYPE ListeLocationArticle IS TABLE OF LigneLocation;
    TYPE ListeElements IS TABLE OF LigneElement;
    PROCEDURE
        SP_01Commande;
    FUNCTION
        FCT_02Amendes(p_numeroMembre IN NUMBER, p_nombreTotalAmende OUT NUMBER) RETURN NUMBER;
    FUNCTION
        FCT_07Best(mois NUMBER, annee NUMBER) RETURN ListeLocationArticle;
    FUNCTION
        FCT_08SommaireLocation(requete VARCHAR2) RETURN ListeElements;
END pkg;

CREATE OR REPLACE PACKAGE BODY pkg AS
    PROCEDURE SP_01Commande IS
    BEGIN
        SAVEPOINT miseAjourCommande;
            UPDATE BI_ARTICLES
            SET INDICATEURENCOMMANDE = '1', QUANTITEENCOMMANDE = (QUANTITEENCOMMANDE + 1)
            WHERE BI_ARTICLES.ISBN =  ANY (SELECT BI_ARTICLES.ISBN
                                      FROM BI_ARTICLES
                                      INNER JOIN BI_EMPRUNTS ON BI_EMPRUNTS.ISBN = BI_ARTICLES.ISBN
                                      WHERE BI_EMPRUNTS.DATEEMPRUNT > ADD_MONTHS(SYSDATE, -1)
                                      GROUP BY BI_ARTICLES.ISBN
                                      HAVING COUNT(BI_EMPRUNTS.ISBN) BETWEEN 6 AND 10);
            UPDATE BI_ARTICLES
            SET INDICATEURENCOMMANDE = '1', QUANTITEENCOMMANDE = (QUANTITEENCOMMANDE + 2)
            WHERE BI_ARTICLES.ISBN =  ANY (SELECT BI_ARTICLES.ISBN
                                      FROM BI_ARTICLES
                                      INNER JOIN BI_EMPRUNTS ON BI_EMPRUNTS.ISBN = BI_ARTICLES.ISBN
                                      WHERE BI_EMPRUNTS.DATEEMPRUNT > ADD_MONTHS(SYSDATE, -24)
                                      GROUP BY BI_ARTICLES.ISBN
                                      HAVING COUNT(BI_EMPRUNTS.ISBN) > 10);
        COMMIT;
        EXCEPTION WHEN OTHERS THEN
        ROLLBACK TO miseAjourCommande;
    END;

    FUNCTION FCT_02Amendes(p_numeroMembre IN NUMBER, p_nombreTotalAmende OUT NUMBER)
    RETURN NUMBER
    AS
    CodeValeur NUMBER(1);
    BEGIN
        SELECT SUM(SUM(NBJOURSDERETARD) * AMENDEPARJOUR)
        INTO p_nombreTotalAmende
        FROM BI_EMPRUNTS
        WHERE NOMEMBRE = p_numeroMembre
        GROUP BY NOMEMBRE, NBJOURSDERETARD, AMENDEPARJOUR;

        SELECT COUNT(NOMEMBRE) INTO CodeValeur FROM BI_MEMBRES WHERE NOMEMBRE = p_numeroMembre;

        IF (CodeValeur = 1) THEN
            IF (p_nombreTotalAmende > 0) THEN
                CodeValeur := 1;
            ELSE
                CodeValeur := 0;
            END IF;
        ELSE
            CodeValeur := 0;
        END IF;

        RETURN CodeValeur;
    END;

    FUNCTION FCT_07Best(mois NUMBER, annee NUMBER)
    RETURN ListeLocationArticle
    IS
    liste ListeLocationArticle;
    CURSOR curseur IS
    SELECT
        LigneLocation(BI_ARTICLES.ISBN
                    ,BI_ARTICLES.typearticle
                    ,BI_ARTICLES.titre
                    ,COUNT(BI_EMPRUNTS.EMPRUNTID))
        FROM BI_ARTICLES
        INNER JOIN BI_COPIESARTICLES
        ON BI_ARTICLES.ISBN = BI_COPIESARTICLES.ISBN
        LEFT JOIN BI_EMPRUNTS
        ON BI_COPIESARTICLES.ISBN = BI_EMPRUNTS.ISBN AND (TO_CHAR(BI_EMPRUNTS.DateEmprunt, 'YYYY') = annee) AND (TO_CHAR(BI_EMPRUNTS.DateEmprunt, 'MM') = mois)
        GROUP BY BI_ARTICLES.ISBN, BI_ARTICLES.titre, BI_ARTICLES.typearticle
        ORDER BY COUNT(BI_EMPRUNTS.EMPRUNTID) DESC;
    BEGIN
        OPEN curseur;
        liste := ListeLocationArticle();
        FETCH curseur
        BULK COLLECT INTO liste;
        CLOSE curseur;
        RETURN liste;
    END;

    FUNCTION FCT_08SommaireLocation(requete VARCHAR2)
    RETURN ListeElements
    IS
        elements ListeElements;
    BEGIN
        elements := listeElements();
        CASE
            WHEN requete = 'Auteur' THEN
                SELECT LigneElement(A.PRENOM || ' ' || A.NOM
                                    ,COUNT(E.EMPRUNTID))
                BULK COLLECT INTO elements
                FROM BI_AUTEURS A
                LEFT JOIN BI_ARTICLESAUTEURS AA
                ON A.AUTEURID = AA.AUTEURID
                LEFT JOIN BI_EMPRUNTS E
                ON E.ISBN = AA.ISBN
                GROUP BY (A.PRENOM || ' ' || A.NOM);
            WHEN requete = 'Article' THEN
                SELECT LigneElement(A.TITRE
                                    ,COUNT(E.EMPRUNTID))
                BULK COLLECT INTO elements
                FROM BI_ARTICLES A
                LEFT JOIN BI_COPIESARTICLES C
                ON A.ISBN = C.ISBN
                LEFT JOIN BI_EMPRUNTS E
                ON C.NOARTICLE = E.NOARTICLE
                GROUP BY A.TITRE;
            WHEN requete = 'Membre' THEN
                SELECT LigneElement(M.PRENOM || ' ' || M.NOM
                                    ,COUNT(E.EMPRUNTID))
                BULK COLLECT INTO elements
                FROM BI_MEMBRES M
                LEFT JOIN BI_EMPRUNTS E
                ON M.NOMEMBRE = E.NOMEMBRE
                GROUP BY M.PRENOM || ' ' || M.NOM;
            ELSE
                INSERT INTO BI_ERREURS VALUES ('FCT_08SOmmaireLocation','Le parametre entre est invalide',SYSDATE);
                RAISE_APPLICATION_ERROR (-20000, 'PARAMÈTRE INVALIDE');
        END CASE;
        RETURN elements;
    END;
END pkg;
/

-- QUESTION #3 (Déclencheur):
CREATE SEQUENCE incrementVentesId
    INCREMENT BY 1
    START WITH 5
    MINVALUE 5
    MAXVALUE 999999
    NOCYCLE;

CREATE OR REPLACE TRIGGER TR_03InsVente
    BEFORE INSERT
    ON BI_VENTES
    FOR EACH ROW
DECLARE
    vVenteId NUMBER(7) DEFAULT incrementVentesId.NEXTVAL;
    vDateVente DATE;
    vVenteIdExistant NUMBER(1);
BEGIN
    vDateVente := :NEW.DATEVENTE;
    SELECT COUNT(*) INTO vVenteIdExistant FROM BI_VENTES WHERE VENTEID = vVenteId;

    IF vDateVente > SYSDATE THEN
        INSERT INTO BI_ERREURS VALUES ('Declencheur Numero 3','La date de la vente ne peut pas être ultérieure à la date du jour.',SYSDATE);
        RAISE_APPLICATION_ERROR (-20000, 'La date de la vente ne peut pas être ultérieure à la date du jour.');
    ELSIF vVenteIdExistant = 1 THEN
        INSERT INTO BI_ERREURS VALUES ('Declencheur Numero 3','L''id de vente existe déjà.',SYSDATE);
        RAISE_APPLICATION_ERROR (-20000, 'L''id de vente existe déjà.');
    END IF;

    :NEW.VENTEID := vVenteId;
    :NEW.TOTALVENTE := 0;
    :NEW.TAXEPROVCOURANTE := 0;
    :NEW.TAXEFEDCOURANTE := 0;
    :NEW.TOTALTAXES := 0;
    :NEW.GRANDTOTALVENTE := 0;
END;
/


-- QUESTION #4 (Déclencheur):
CREATE OR REPLACE TRIGGER TR_04InsVente
    BEFORE INSERT OR UPDATE OR DELETE ON BI_VENTESPRODUITS
    FOR EACH ROW
        DECLARE
            v_IndicateurTaxe CHAR;
            v_PrixUnitaire NUMBER(5,2);
            v_totalAchatProduitSansTaxes NUMBER(5,2);
            v_pcTaxeProvCourante NUMBER(5,3);
            v_TaxeProvCourante NUMBER(5,3);
            v_pcTaxeFedCourante NUMBER (5,3);
            v_TaxeFedCourante NUMBER(5,3);
            v_TotalTaxesProduit NUMBER(7,2);
            v_GrandTotalVenteProduit NUMBER(7,2);
        BEGIN
            IF DELETING OR UPDATING THEN
                SELECT PCTAXEPROV INTO v_pcTaxeProvCourante FROM BI_PROVINCES WHERE PROVCODE =
                              (SELECT PROVCODE FROM BI_MEMBRES INNER JOIN BI_VENTES ON BI_MEMBRES.NOMEMBRE = BI_VENTES.NOMEMBRE WHERE BI_VENTES.VENTEID = :OLD.VENTEID);
                SELECT PCTAXEFED INTO v_pcTaxeFedCourante FROM BI_PROVINCES WHERE PROVCODE =
                              (SELECT PROVCODE FROM BI_MEMBRES INNER JOIN BI_VENTES ON BI_MEMBRES.NOMEMBRE = BI_VENTES.NOMEMBRE WHERE BI_VENTES.VENTEID = :OLD.VENTEID);
                v_IndicateurTaxe := :OLD.INDICATEURTAXABLE;
                v_PrixUnitaire := :OLD.PRIXUNITAIRE;
                v_totalAchatProduitSansTaxes := :OLD.TOTALACHATPRODUIT;
                IF (v_IndicateurTaxe = '1') THEN
                    v_TaxeProvCourante := v_totalAchatProduitSansTaxes * v_pcTaxeProvCourante;
                    v_TaxeFedCourante := v_totalAchatProduitSansTaxes * v_pcTaxeFedCourante;
                    v_TotalTaxesProduit := v_TaxeProvCourante + v_TaxeFedCourante;
                    v_GrandTotalVenteProduit := v_totalAchatProduitSansTaxes + v_TotalTaxesProduit;
                ELSIF (v_IndicateurTaxe = '0') THEN
                    v_TaxeProvCourante := 0;
                    v_TaxeFedCourante := 0;
                    v_TotalTaxesProduit := 0;
                    v_GrandTotalVenteProduit := v_totalAchatProduitSansTaxes + v_TotalTaxesProduit;
                END IF;

                UPDATE BI_VENTES
                SET TotalVente = TotalVente - v_totalAchatProduitSansTaxes,
                    TaxeProvCourante = TaxeProvCourante - v_TaxeProvCourante,
                    TaxeFedCourante =  TaxeFedCourante - v_TaxeFedCourante,
                    TotalTaxes =  TotalTaxes - v_TotalTaxesProduit,
                    GrandTotalVente = GrandTotalVente - v_GrandTotalVenteProduit
                WHERE VENTEID = :OLD.VENTEID;
            END IF;

            IF ( (INSERTING OR UPDATING) AND :NEW.VENTEID IS NOT NULL AND :NEW.CODEPRODUIT IS NOT NULL AND :NEW.QTEACHETEE IS NOT NULL) THEN
                -- Assignation de valeurs aux variables
                SELECT INDICATEURTAXABLE INTO v_IndicateurTaxe FROM BI_PRODUITS WHERE CODEPRODUIT = :NEW.CODEPRODUIT;
                SELECT PRIXUNITAIRE INTO v_PrixUnitaire FROM BI_PRODUITS WHERE CODEPRODUIT = :NEW.CODEPRODUIT;
                SELECT PCTAXEPROV INTO v_pcTaxeProvCourante FROM BI_PROVINCES WHERE PROVCODE =
                              (SELECT PROVCODE FROM BI_MEMBRES INNER JOIN BI_VENTES ON BI_MEMBRES.NOMEMBRE = BI_VENTES.NOMEMBRE WHERE BI_VENTES.VENTEID = :NEW.VENTEID);
                SELECT PCTAXEFED INTO v_pcTaxeFedCourante FROM BI_PROVINCES WHERE PROVCODE =
                              (SELECT PROVCODE FROM BI_MEMBRES INNER JOIN BI_VENTES ON BI_MEMBRES.NOMEMBRE = BI_VENTES.NOMEMBRE WHERE BI_VENTES.VENTEID = :NEW.VENTEID);
                --Determination des taxes selon l'indicateurTaxable
                v_totalAchatProduitSansTaxes := ROUND(:NEW.QTEACHETEE * v_PrixUnitaire,2);
                IF (v_IndicateurTaxe = '1') THEN
                    v_TaxeProvCourante := v_totalAchatProduitSansTaxes * v_pcTaxeProvCourante;
                    v_TaxeFedCourante := v_totalAchatProduitSansTaxes * v_pcTaxeFedCourante;
                    v_TotalTaxesProduit := v_TaxeProvCourante + v_TaxeFedCourante;
                    v_GrandTotalVenteProduit := v_totalAchatProduitSansTaxes + v_TotalTaxesProduit;
                ELSIF (v_IndicateurTaxe = '0') THEN
                    v_TaxeProvCourante := 0;
                    v_TaxeFedCourante := 0;
                    v_TotalTaxesProduit := 0;
                    v_GrandTotalVenteProduit := v_totalAchatProduitSansTaxes + v_TotalTaxesProduit;
                END IF;

                -- Assignation des bonnes valeurs aux bons champs
                :NEW.INDICATEURTAXABLE := v_IndicateurTaxe;
                :NEW.PRIXUNITAIRE := v_PrixUnitaire;
                :NEW.TOTALACHATPRODUIT := v_totalAchatProduitSansTaxes;

                -- Mise a jour de la table BI_VENTES en fonction du venteId
                UPDATE BI_VENTES
                SET TotalVente = (nvl(TotalVente,0)) + v_totalAchatProduitSansTaxes,
                    TaxeProvCourante = (nvl(TaxeProvCourante,0)) + v_TaxeProvCourante,
                    TaxeFedCourante = (nvl(TaxeFedCourante,0)) + v_TaxeFedCourante,
                    TotalTaxes = (nvl(TotalTaxes,0)) + v_TotalTaxesProduit,
                    GrandTotalVente = (nvl(GrandTotalVente,0)) + v_GrandTotalVenteProduit
                WHERE VENTEID = :NEW.VENTEID;

            END IF;
        END;
        /

-- Question #5
CREATE OR REPLACE TRIGGER TR_05Emprunt
    BEFORE INSERT OR UPDATE OR DELETE ON BI_EMPRUNTS
    FOR EACH ROW
    DECLARE
        v_AmendeParJour NUMBER(3,2);
        v_ISBN CHAR(17);
        v_nbJourSurEmprunt NUMBER;
        v_TypeMembre VARCHAR(20);
        v_nbJourSurEmpruntSelonTypeMembre NUMBER(2);
        v_TypeArticle VARCHAR(20);
        v_indicateurDisponibilite CHAR(1);

        v_ancienCommentaireID NUMBER(7);
        v_ancienCommentaire VARCHAR2(250);
        v_ancienDateCommentaire DATE;
    BEGIN
       IF INSERTING THEN
            SELECT ISBN INTO v_ISBN FROM BI_COPIESARTICLES WHERE NOARTICLE = :NEW.NOARTICLE;
            SELECT TYPEARTICLE INTO v_TypeArticle FROM BI_ARTICLES WHERE ISBN = v_ISBN;
            SELECT TYPEMEMBRE INTO v_TypeMembre FROM BI_MEMBRES WHERE NOMEMBRE = :NEW.NOMEMBRE;
            SELECT AMENDEPARJOUR INTO v_AmendeParJour FROM BI_TYPEARTICLES WHERE TYPEARTICLE = v_TypeArticle;
            SELECT NBJOURSSUREMPRUNT INTO v_nbJourSurEmpruntSelonTypeMembre FROM BI_TYPESMEMBRES WHERE TYPEMEMBRE = v_TypeMembre;
            SELECT INDICATEURDISPONIBLE INTO v_indicateurDisponibilite FROM BI_COPIESARTICLES WHERE NOARTICLE = :NEW.NOARTICLE;

            IF ((v_TypeArticle = 'DVD') OR (v_TypeArticle = 'BLU')) THEN v_nbJourSurEmprunt := 7;
                ELSIF (v_TypeArticle = 'JEU') THEN v_nbJourSurEmprunt := 10;
                ELSE v_nbJourSurEmprunt := v_nbJourSurEmpruntSelonTypeMembre;
            END IF;

            IF (v_indicateurDisponibilite = '0') THEN
                RAISE_APPLICATION_ERROR(-20000, 'L''article n''est pas disponible présentement et donc il ne peut pas être emprunté');
            END IF;

            :NEW.EMPRUNTID := SEQ_EMPRUNTID.nextval;
            :NEW.DATERETOURPREVUE := (SYSDATE + v_nbJourSurEmprunt);
            :NEW.NBJOURSDERETARD := 0;
            :NEW.AMENDEPARJOUR := v_AmendeParJour;
            :NEW.INDICATEURPERTE := '0';
            :NEW.TOTALAMENDE := 0;
            :NEW.MODEPAIEMENTCD := null;
            :NEW.ISBN := v_ISBN;

            UPDATE BI_COPIESARTICLES SET INDICATEURDISPONIBLE = '0' WHERE NOARTICLE = :NEW.NOARTICLE;

       ELSIF (UPDATING AND :NEW.DATERETOUR IS NOT NULL) THEN
            IF((:NEW.DATERETOUR - :NEW.DATERETOURPREVUE) > 0) THEN
                :NEW.NBJOURSDERETARD := (:NEW.DATERETOUR - :NEW.DATERETOURPREVUE);
                :NEW.TOTALAMENDE := (:NEW.DATERETOUR - :NEW.DATERETOURPREVUE) * :NEW.AMENDEPARJOUR;
            ELSE
                :NEW.NBJOURSDERETARD := 0;
                :NEW.TOTALAMENDE := 0;
            END IF;

            UPDATE BI_COPIESARTICLES SET INDICATEURDISPONIBLE = '1' WHERE NOARTICLE = :OLD.NOARTICLE;

       ELSIF DELETING THEN
            SELECT (SELECT COMMENTAIREID FROM BI_COMMENTAIRES WHERE EMPRUNTID = :OLD.EMPRUNTID) INTO v_ancienCommentaireID FROM DUAL;
            SELECT (SELECT COMMENTAIRE FROM BI_COMMENTAIRES WHERE EMPRUNTID = :OLD.EMPRUNTID) INTO v_ancienCommentaire FROM DUAL;
            SELECT (SELECT DATECOMMENTAIRE FROM BI_COMMENTAIRES WHERE EMPRUNTID = :OLD.EMPRUNTID) INTO v_ancienDateCommentaire FROM DUAL;
            IF(v_ancienDateCommentaire IS NOT NULL AND v_ancienCommentaire IS NOT NULL AND v_ancienCommentaireID IS NOT NULL) THEN
                INSERT INTO BI_HISTORIQUE_COMMENTAIRES (HIST_COMMENTAIREID, HIST_EMPRUNTID, HIST_COMMENTAIRE, HIST_DATECOMMENTAIRE)
                VALUES(v_ancienCommentaireID, :OLD.EMPRUNTID, v_ancienCommentaire, v_ancienDateCommentaire);
            END IF;
       END IF;
    END;
    /

-- QUESTION #6 (Déclencheur):

CREATE OR REPLACE PROCEDURE AjouterLog(p_exec VARCHAR, p_msg VARCHAR, p_date DATE ) IS
BEGIN
    INSERT INTO BI_ERREURS(execution, message, dateapparition) VALUES (p_exec, p_msg, p_date);
END;

CREATE OR REPLACE TRIGGER TR_06Location
    BEFORE INSERT
    ON BI_EMPRUNTS
    FOR EACH ROW
DECLARE
    vIndicateurDisponible CHAR(1);
    vNombreArticleLoueEnCours NUMBER;
BEGIN
    SELECT INDICATEURDISPONIBLE INTO vIndicateurDisponible FROM BI_COPIESARTICLES WHERE NOARTICLE = :NEW.NOARTICLE;
    SELECT COUNT(*) INTO vNombreArticleLoueEnCours FROM BI_EMPRUNTS WHERE NOMEMBRE = :NEW.NOMEMBRE AND DATERETOUR IS NULL;
    IF vNombreArticleLoueEnCours >= 10 THEN
        AjouterLog(-20000, 'Le nombre maximum d''article pouvant être loué a été atteint.', SYSDATE);
        RAISE_APPLICATION_ERROR (-20000, 'Le nombre maximum d''article pouvant être loué a été atteint.');
    ELSIF vIndicateurDisponible = '0' THEN
        INSERT INTO BI_ERREURS VALUES ('Declencheur Numero 6','L''artcile n''est pas disponible',SYSDATE);
        AjouterLog('Declencheur Numero 6','L''artcile n''est pas disponible',SYSDATE);
        RAISE_APPLICATION_ERROR (-20000, 'L''article n''est pas disponible.');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER TR_06AjoutLog
    BEFORE INSERT
    ON BI_EMPRUNTS
    FOR EACH ROW
DECLARE
    vIndicateurDisponible CHAR(1);
    vNombreArticleLoueEnCours NUMBER;
BEGIN
    SELECT INDICATEURDISPONIBLE INTO vIndicateurDisponible FROM BI_COPIESARTICLES WHERE NOARTICLE = :NEW.NOARTICLE;
    SELECT COUNT(*) INTO vNombreArticleLoueEnCours FROM BI_EMPRUNTS WHERE NOMEMBRE = :NEW.NOMEMBRE AND DATERETOUR IS NULL;
    IF vNombreArticleLoueEnCours >= 10 THEN
        AjouterLog(-20000, 'Le nombre maximum d''article pouvant être loué a été atteint.', SYSDATE);
    ELSIF vIndicateurDisponible = '0' THEN
        AjouterLog('Declencheur Numero 6','L''artcile n''est pas disponible',SYSDATE);
    END IF;
END;
/


-- *** TEST AJOUT ERREUR
SELECT * FROM BI_EMPRUNTS;
INSERT INTO BI_EMPRUNTS(nomembre, noarticle) VALUES (1,2);
SELECT * FROM BI_ERREURS;

-- Function de test pour les ajouts
CREATE OR REPLACE PROCEDURE AjouterLog(p_exec VARCHAR, p_msg VARCHAR, p_date DATE ) IS
BEGIN
    INSERT INTO BI_ERREURS(execution, message, dateapparition) VALUES (p_exec, p_msg, p_date);
    COMMIT;
END AjouterLog;

/* Question 9:
Fonction pour lister les auteurs d'un article: */

CREATE OR REPLACE TYPE Auteur IS OBJECT(
    auteurId NUMBER
    ,nom VARCHAR2(50)
    ,prenom VARCHAR2(50)
);
/

CREATE OR REPLACE TYPE ListeAuteur IS TABLE OF Auteur;

CREATE OR REPLACE FUNCTION fct_ListerAuteurs( p_ISBN CHAR )
RETURN ListeAuteur
IS
    liste ListeAuteur;
BEGIN
    SELECT Auteur(A.auteurid, A.nom, A.prenom)
    BULK COLLECT INTO liste
    FROM BI_AUTEURS A
    INNER JOIN BI_ARTICLESAUTEURS AA
    ON A.auteurid = AA.auteurid
    WHERE AA.ISBN = p_ISBN;
    RETURN liste;
END;
/

-- Fonction pour v�rifier le numero membre, l'ISBN et renvoyer un objet de type emprunt avec le numero article:

CREATE OR REPLACE TYPE Emprunt IS OBJECT(
    NoMembre NUMBER
    ,ISBN CHAR(17)
    ,NoArticle NUMBER
)
/

CREATE OR REPLACE TYPE ListeEmprunt IS TABLE OF Emprunt;

CREATE OR REPLACE FUNCTION fct_CreerModeleEmprunt(p_numeroMembre NUMBER, p_ISBN CHAR)
RETURN ListeEmprunt
IS

    empruntCree ListeEmprunt;
    numeroArticleValide NUMBER;
    numeroArticle NUMBER;
    numeroMembreValide NUMBER;
    noMembre NUMBER;
    ISBNValide NUMBER;
    ISBN CHAR(17);
    
BEGIN

    empruntCree := ListeEmprunt();
    
    SELECT COUNT(NOMEMBRE) INTO numeroMembreValide FROM BI_MEMBRES WHERE NOMEMBRE = p_numeroMembre;    
    IF numeroMembreValide > 0 THEN
        noMembre := p_numeroMembre;
    ELSE
        noMembre := 0;
    END IF;
    
    SELECT COUNT(ISBN) INTO ISBNValide FROM BI_COPIESARTICLES WHERE ISBN = p_ISBN;
    IF ISBNValide > 0 THEN
        ISBN := p_ISBN;
        
        SELECT COUNT(NOARTICLE) INTO numeroArticleValide FROM BI_COPIESARTICLES WHERE ISBN = p_ISBN AND INDICATEURDISPONIBLE = '1';
        IF numeroArticleValide > 0 THEN
            SELECT NOARTICLE INTO numeroArticle FROM BI_COPIESARTICLES WHERE ISBN = p_ISBN AND INDICATEURDISPONIBLE = '1';
        ELSE
            numeroArticle := 0;
        END IF;    

    ELSE
       ISBN := '000-0-00000-000-0';
       numeroArticle := 0;
    END IF;

    empruntCree.EXTEND;
    empruntCree(1) := Emprunt(noMembre, ISBN, numeroArticle);
    
    RETURN empruntCree;
END;
/
