﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeEntite
{
    public interface IDepotArticle
    {
        public List<Article> ListerArticles();
        public List<Location> ListerLocations(int p_mois, int p_annee);
    }
}
