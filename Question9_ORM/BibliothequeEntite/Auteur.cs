﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeEntite
{
    public class Auteur
    {
        public int AuteurId { get; private set; }
        public string Nom { get; private set; }
        public string Prenom { get; private set; }

        public Auteur(int  p_id, string p_nom, string p_prenom)
        {
            if(p_id < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_id));
            }
            if(p_nom == null)
            {
                throw new ArgumentNullException(nameof(p_nom));
            }
            if(p_prenom == null)
            {
                throw new ArgumentNullException(nameof(p_prenom));
            }

            this.AuteurId = p_id;
            this.Nom = p_nom;
            this.Prenom = p_prenom;
        }
        public override string ToString()
        {
            return $"\r\n\tAuteur: \r\n\t{this.Prenom} {this.Nom}";
        }
    }
}
