﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BibliothequeEntite;

namespace BibliothequeBL
{
    public class ArticleBL
    {
        private BibliothequeEntite.IDepotArticle m_DepotArticle;
        public ArticleBL(BibliothequeEntite.IDepotArticle p_depotArticle)
        {
            this.m_DepotArticle = p_depotArticle;
        }
        public List<BibliothequeEntite.Article> ListerArticles()
        {
            return this.m_DepotArticle.ListerArticles();
        }
        public List<Location> ObtenirNombreDeLocations(int p_mois, int p_annee)
        {
            if (p_mois < 0 || p_mois > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(p_mois));
            }
            if (p_annee < 1900 || p_annee > DateTime.Now.Year)
            {
                throw new ArgumentOutOfRangeException(nameof(p_annee));
            }

            return this.m_DepotArticle.ListerLocations(p_mois, p_annee);
        }
    }
}
