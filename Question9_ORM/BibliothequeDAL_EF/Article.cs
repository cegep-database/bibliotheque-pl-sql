﻿using System;
using Oracle.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.Json;
using BibliothequeEntite;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace BibliothequeDAL_EF
{
    [Table("BI_ARTICLES")]
    public class Article
    {
        [Key]
        [Column("ISBN")]
        public string ISBN { get; private set; }
        [Column("TYPEARTICLE")]
        public string TypeArticle { get; private set; }
        [Column("TITRE")]
        public string Titre { get; private set; }
        [Column("DATEPARUTION")]
        public string DateParution { get; private set; }
        public List<BibliothequeEntite.Auteur> ListeAuteurs;
        public Article()
        {
            ;
        }
        public Article(BibliothequeEntite.Article p_article)
        {
            if(p_article == null)
            {
                throw new ArgumentNullException(nameof(p_article));
            }

            this.ISBN = p_article.ISBN;
            this.TypeArticle = p_article.TypeArticle;
            this.Titre = p_article.Titre;
            this.DateParution = p_article.DateParution;
            this.ListeAuteurs = p_article.ListeAuteurs;
        }
        public BibliothequeEntite.Article VersEntite()
        {
            return new BibliothequeEntite.Article(this.ISBN, this.TypeArticle, this.Titre, this.DateParution, this.ListeAuteurs);
        }
    }
}
