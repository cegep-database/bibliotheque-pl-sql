﻿using System;
using Oracle.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.Json;
using BibliothequeEntite;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace BibliothequeDAL_EF
{
    [Table("BI_AUTEURS")]
    public class Auteur
    {
        [Key]
        [Column("AUTEURID")]
        public int AuteurId { get; private set; }
        [Column("NOM")]
        public string Nom { get; private set; }
        [Column("PRENOM")]
        public string Prenom { get; private set; }

        public Auteur()
        {
            ;
        }
        public Auteur(BibliothequeEntite.Auteur p_auteur)
        {
            if(p_auteur == null)
            {
                throw new ArgumentNullException(nameof(p_auteur));
            }

            this.AuteurId = p_auteur.AuteurId;
            this.Nom = p_auteur.Nom;
            this.Prenom = p_auteur.Prenom;
        }
        public BibliothequeEntite.Auteur VersEntite()
        {
            return new BibliothequeEntite.Auteur(this.AuteurId, this.Nom, this.Prenom);
        }
    }
}
