﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BibliothequeDAL_EF
{
    public class DepotAuteur : BibliothequeEntite.IDepotAuteur
    {
        private ApplicationDBContext m_dbContext;

        public DepotAuteur(ApplicationDBContext p_applicationDBContext)
        {
            if (p_applicationDBContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDBContext));
            }

            this.m_dbContext = p_applicationDBContext;
        }

        public List<BibliothequeEntite.Auteur> ListerAuteurs()
        {
            IQueryable<Auteur> requete = this.m_dbContext.Auteurs;
            return requete.Select(auteur => auteur.VersEntite()).ToList();
        }
    }
}
