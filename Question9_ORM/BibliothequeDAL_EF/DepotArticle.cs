﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BibliothequeDAL_EF
{
    public class DepotArticle : BibliothequeEntite.IDepotArticle
    {
        private ApplicationDBContext m_dbContext;

        public DepotArticle(ApplicationDBContext p_applicationDBContext)
        {
            if (p_applicationDBContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDBContext));
            }

            this.m_dbContext = p_applicationDBContext;
        }

        public List<BibliothequeEntite.Article> ListerArticles()
        {
            IQueryable<Article> requete = this.m_dbContext.Articles;
            List<BibliothequeEntite.Article> listeArticles = requete.Select(article => article.VersEntite()).ToList();
            foreach (BibliothequeEntite.Article article in listeArticles)
            {
                article.ListeAuteurs = this.m_dbContext.ObtenirAuteursDeLArticle(article.ISBN)?.Select(auteur => auteur.VersEntite()).ToList();
            }
            return listeArticles;
        }
        public List<BibliothequeEntite.Location> ListerLocations(int p_mois, int p_annee)
        {
            if (p_mois < 0 || p_mois > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(p_mois));
            }
            if (p_annee < 1900 || p_annee > DateTime.Now.Year)
            {
                throw new ArgumentOutOfRangeException(nameof(p_annee));
            }

            List<Location> requete = this.m_dbContext.ObtenirNombreDeLocations(p_mois, p_annee);
            List<BibliothequeEntite.Location> listeLocations = requete.Select(location => location.VersEntite()).ToList();
            return listeLocations;
        }
    }
}
