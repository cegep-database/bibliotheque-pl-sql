﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeEntite
{
    public class Emprunt
    {
        public int NoMembre { get; private set; }
        public string ISBN { get; private set; }
        public int NoArticle { get; private set; }
        public Emprunt(int p_noMembre, string p_ISBN, int p_noArticle)
        {
            if(p_noMembre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_noMembre));
            }
            if(p_ISBN == null)
            {
                throw new ArgumentNullException(nameof(p_ISBN));
            }
            if(p_noArticle < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_noArticle));
            }
            this.NoMembre = p_noMembre;
            this.ISBN = p_ISBN;
            this.NoArticle = p_noArticle;
        }
    }
}
