﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeEntite
{
    public class Article
    {
        public string ISBN { get; private set; }
        public string TypeArticle { get; private set; }
        public string Titre { get; private set; }
        public string? DateParution { get; private set; }
        private List<Auteur>? m_auteurs;
        public List<Auteur>? ListeAuteurs
        {
            get
            {
                return m_auteurs;
            }
            set
            {
                this.m_auteurs = value;
            }
        }
        public Article(string p_ISBN, string p_typeArticle, string p_titre)
        {
            if (p_ISBN == null)
            {
                throw new ArgumentNullException(nameof(p_ISBN));
            }
            if (p_typeArticle == null)
            {
                throw new ArgumentNullException(nameof(p_typeArticle));
            }
            if (p_titre == null)
            {
                throw new ArgumentNullException(nameof(p_titre));
            }
            this.ISBN = p_ISBN;
            this.TypeArticle = p_typeArticle;
            this.Titre = p_titre;
        }
        public Article(string p_ISBN, string p_typeArticle, string p_titre, string p_dateParution, List<Auteur>? p_listeAuteurs) : this(p_ISBN, p_typeArticle, p_titre)
        {
            if(p_dateParution == null)
            {
                throw new ArgumentNullException(nameof(p_dateParution));
            }

            this.DateParution = p_dateParution;
            this.ListeAuteurs = p_listeAuteurs;
        }
        public override string ToString()
        {
            string article = $"Article:\r\n{this.ISBN}\r\n{this.TypeArticle}\r\n{this.Titre}\r\n{(this.DateParution is null ? "" : this.DateParution)}";
            if (this.ListeAuteurs != null)
            {
                article += Environment.NewLine + "Auteurs:";

                foreach (Auteur auteur in this.ListeAuteurs)
                {
                    article += Environment.NewLine + auteur.ToString();
                }

                article += Environment.NewLine;
            }
            return article;
        }
    }
}