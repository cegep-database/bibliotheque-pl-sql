﻿using BibliothequeBL;
using BibliothequeDAL_EF;
using BibliothequeEntite;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace BibliothequeUI
{
    class Program
    {
        static void Main(string[] args)
        {

            using (ApplicationDBContext dbContext = DALDbContextGeneration.ObtenirApplicationDBContext())
            {
                ArticleBL articleBL = new ArticleBL(new DepotArticle(dbContext));
                AuteurBL auteurBL = new AuteurBL(new DepotAuteur(dbContext));
                EmpruntBL empruntBL = new EmpruntBL(new DepotEmprunt(dbContext));

                BibliothequeUI bibliothequeUI = new BibliothequeUI(articleBL, auteurBL, empruntBL);
                bibliothequeUI.ExecuterUI();
            }
        }
    }
}
