﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeEntite
{
    public class Location
    {
        public Article Article { get; private set; }
        public int Nombre { get; private set; }
        public Location(Article p_article, int p_nombre)
        {
            if(p_article == null)
            {
                throw new ArgumentNullException(nameof(p_article));
            }
            if(p_nombre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_nombre));
            }
            this.Article = p_article;
            this.Nombre = p_nombre;
        }
        public override string ToString()
        {
            return $"Article:\r\n\tISBN: {this.Article.ISBN}\r\n\tType: {this.Article.TypeArticle}\r\n\tTitre: {this.Article.Titre}\r\n\tNombre d'emprunts: {this.Nombre}\r\n";
        }
    }
}
