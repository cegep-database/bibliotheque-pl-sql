﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeEntite
{
    public interface IDepotEmprunt
    {
        public void AjouterEmprunt(BibliothequeEntite.Emprunt p_emprunt);
        public Emprunt CreerEmprunt(int p_numeroMembre, string p_ISBN);
    }
}
