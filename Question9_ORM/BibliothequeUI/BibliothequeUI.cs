﻿using BibliothequeBL;
using BibliothequeEntite;
using BibliothequeDAL_EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliothequeUI
{
    public class BibliothequeUI
    {
        private BibliothequeBL.ArticleBL m_articles;
        private BibliothequeBL.AuteurBL m_auteurs;
        private BibliothequeBL.EmpruntBL m_emprunts;
        public BibliothequeUI(BibliothequeBL.ArticleBL p_articles, AuteurBL p_auteurs, EmpruntBL p_emprunts)
        {
            this.m_articles = p_articles;
            this.m_auteurs = p_auteurs;
            this.m_emprunts = p_emprunts;
        }
        public void ExecuterUI()
        {
            bool programmeEnExecution = true;

            while (programmeEnExecution)
            {
                this.AfficherMenu();
                int choixMenu = this.SaisirChoixMenu();

                switch (choixMenu)
                {
                    case 1:
                        this.ListerEtAfficherArticles();
                        break;
                    case 2:
                        this.ListerEtAfficherAuteurs();
                        break;
                    case 3:
                        this.ListerEtAfficherLocations();
                        break;
                    case 4:
                        this.AjouterEmprunt();
                        break;
                    case 0:
                        programmeEnExecution = false;
                        break;
                    default:
                        break;
                }
            }
        }
        public void AfficherMenu()
        {
            Console.Out.WriteLine("MENU PRINCIPAL");
            Console.Out.WriteLine("[1] Lister les articles");
            Console.Out.WriteLine("[2] Lister les auteurs");
            Console.Out.WriteLine("[3] Lister les locations pour un mois et une année donnés");
            Console.Out.WriteLine("[4] Ajouter un emprunt");
            Console.Out.WriteLine("[0] Quitter");
        }
        public int SaisirChoixMenu()
        {
            int choix = -1;
            while (choix < 0 || choix > 6)
            {
                Console.Out.WriteLine("Entrez votre choix, de 0 a 4:");
                choix = Convert.ToInt32(Console.In.ReadLine());
            }
            return choix;
        }
        public void AfficherArticle(BibliothequeEntite.Article p_article)
        {
            Console.Out.WriteLine(p_article.ToString());
        }
        public void ListerEtAfficherArticles()
        {
            List<BibliothequeEntite.Article> listeDesArticles = this.m_articles.ListerArticles();
            listeDesArticles.ForEach(article => this.AfficherArticle(article));
        }
        public void AfficherAuteur(BibliothequeEntite.Auteur p_auteur)
        {
            Console.Out.WriteLine(p_auteur.ToString());
        }
        public void ListerEtAfficherAuteurs()
        {
            List<BibliothequeEntite.Auteur> listeDesAuteurs = this.m_auteurs.ListerAuteurs();
            listeDesAuteurs.ForEach(auteur => this.AfficherAuteur(auteur));
        }
        public void AfficherLocation(BibliothequeEntite.Location p_location)
        {
            Console.Out.WriteLine(p_location.ToString());
        }
        public void ListerEtAfficherLocations()
        {
            int mois = this.SaisirMois();
            int annee = this.SaisirAnnee();
            List<BibliothequeEntite.Location> listeDesLocation = this.m_articles.ObtenirNombreDeLocations(mois, annee);
            listeDesLocation.ForEach(location => this.AfficherLocation(location));
        }
        public int SaisirMois()
        {
            int mois = 0;
            string reponse = "";
            bool moisValide = false;
            while(!moisValide)
            {
                Console.Out.Write("Veuillez saisir le mois: ");
                reponse = Console.In.ReadLine();
                try
                {
                    mois = int.Parse(reponse);
                    if(mois > 0 && mois < 13)
                    {
                        moisValide = true;
                    }
                    else
                    {
                        Console.Out.WriteLine("Vous devez entrer un mois valide entre 1 et 12!");
                    }
                }
                catch(FormatException)
                {
                    Console.Out.WriteLine("Le mois doit être un nombre entier!");
                }
            }
            return mois;
        }
        public int SaisirAnnee()
        {
            int annee = 0;
            string reponse = "";
            bool anneeValide = false;
            while (!anneeValide)
            {
                Console.Out.Write("Veuillez saisir l'annee: ");
                reponse = Console.In.ReadLine();
                try
                {
                    annee = int.Parse(reponse);
                    if (annee < DateTime.Now.Year)
                    {
                        anneeValide = true;
                    }
                    else
                    {
                        Console.Out.WriteLine("Vous devez entrer une année en cours ou passée!");
                    }
                }
                catch (FormatException)
                {
                    Console.Out.WriteLine("L'année doit être un nombre entier!");
                }
            }
            return annee;
        }
        public void AjouterEmprunt()
        {
            int numeroMembre = this.SaisirNumeroMembre();
            string ISBN = this.SaisirISBN();
            BibliothequeEntite.Emprunt emprunt = this.m_emprunts.CreerEmprunt(numeroMembre, ISBN);

            if (emprunt != null)
            {
                if(emprunt.NoMembre == 0)
                {
                    Console.Out.WriteLine("Le numero de membre choisi n'existe pas!");
                }
                else if(emprunt.ISBN == "000-0-00000-000-0")
                {
                    Console.Out.WriteLine("Le numero ISBN choisi n'existe pas!");
                }
                else if(emprunt.NoArticle == 0)
                {
                    Console.Out.WriteLine("L'article n'est pas disponible!");
                }
                else
                {
                    this.m_emprunts.AjouterEmprunt(emprunt);
                    Console.Out.WriteLine("Emprunt ajouté.");
                }
            }            
        }
        public int SaisirNumeroMembre()
        {
            int numeroMembre = 0;
            string reponse = "";
            bool numeroMembreValide = false;
            while (!numeroMembreValide)
            {
                Console.Out.Write("Veuillez saisir le numero du membre: ");
                reponse = Console.In.ReadLine();
                try
                {
                    numeroMembre = int.Parse(reponse);
                    numeroMembreValide = true;
                }
                catch (FormatException)
                {
                    Console.Out.WriteLine("Le numéro de membre doit être un nombre entier!");
                }
            }
            return numeroMembre;
        }
        public string SaisirISBN()
        {
            string ISBN = "";
            bool ISBNValide = false;
            while (!ISBNValide)
            {
                Console.Out.Write("Veuillez saisir le numero ISBN de l'article: ");
                ISBN = Console.In.ReadLine();
                
                if (ISBN.Length == 17)
                {
                    ISBNValide = true;
                }
                else
                {
                    Console.Out.WriteLine("Vous devez entrer un numéro ISBN valide!");
                }
            }
            return ISBN;
        }
    }
}
